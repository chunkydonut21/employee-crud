import React, { Component } from 'react'

class Header extends Component {
    render() {
        return (
            <div className="header">
                <h1 style={{ color: '#212121' }}>EMPLOYEE CRUD</h1>
            </div>
        )
    }
}

export default Header
