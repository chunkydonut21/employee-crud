import React from 'react'
import { Field, reduxForm, reset } from 'redux-form'
import { Button, Form, FormGroup, Label, Input } from 'reactstrap'
import { connect } from 'react-redux'
import { updateEmployee } from '../actions/index'

import { Collapse } from 'reactstrap'

const renderField = ({ input, label, type, placeholder, meta: { touched, error } }) => {
    return (
        <FormGroup>
            <Label for={label}>{label}</Label>
            <Input {...input} invalid={touched && error ? true : false} type={type} placeholder={placeholder} />
        </FormGroup>
    )
}

class UpdateForm extends React.Component {
    constructor(props) {
        super(props)
        this.submit = this.submit.bind(this)
        this.toggle = this.toggle.bind(this)
        this.state = { collapse: false }
    }

    toggle() {
        this.setState({ collapse: !this.state.collapse })
    }

    submit({ name, age, email, address, experience, salary, doj }) {
        const val = {}
        val.id = this.props.emp
        val.name = name
        val.age = age
        val.email = email
        val.address = address
        val.experience = experience
        val.salary = salary
        val.doj = doj

        this.props.updateEmployee(val)
        this.setState({ collapse: false })
    }

    render() {
        return (
            <div className="mt-1">
                <Button
                    color="primary"
                    onClick={this.toggle}
                    style={{ marginBottom: '1rem' }}
                    className="btn btn-block"
                >
                    Update Employee
                </Button>

                <Collapse isOpen={this.state.collapse}>
                    <Form onSubmit={this.props.handleSubmit(this.submit)}>
                        <Field name="name" component={renderField} type="text" label="Name" placeholder="Your name" />
                        <Field name="age" component={renderField} type="number" label="Age" placeholder="Your age" />
                        <Field
                            name="email"
                            component={renderField}
                            type="email"
                            label="Email"
                            placeholder="Your email"
                        />
                        <Field
                            name="address"
                            component={renderField}
                            type="text"
                            label="Address"
                            placeholder="Your Address"
                        />
                        <Field
                            name="experience"
                            component={renderField}
                            type="text"
                            label="Work History"
                            placeholder="Your Work Experience"
                        />
                        <Field name="salary" component={renderField} type="text" label="Salary" placeholder="Salary" />
                        <Field name="doj" component={renderField} type="date" label="Date of Joining" />

                        <Button type="submit">Submit</Button>
                    </Form>
                </Collapse>
            </div>
        )
    }
}

function validate(values) {
    const errors = {}

    if (!values.name) {
        errors.name = 'Required'
    }

    if (!values.age) {
        errors.age = 'Required'
    }
    if (!values.email) {
        errors.email = 'Required'
    }

    if (!values.address) {
        errors.address = 'Required'
    }
    if (!values.salary) {
        errors.salary = 'Required'
    }

    if (!values.doj) {
        errors.doj = 'Required'
    }

    return errors
}

const afterSubmit = (result, dispatch) => dispatch(reset('review'))

export default connect(
    null,
    { updateEmployee }
)(reduxForm({ form: 'review', onSubmitSuccess: afterSubmit, validate })(UpdateForm))
