import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Card, Button, CardHeader, CardBody, CardText } from 'reactstrap'
import { removeEmployee } from '../actions/index'
import UpdateForm from './UpdateForm'

class EmployeeList extends Component {
    constructor(props) {
        super(props)
        this.toggle = this.toggle.bind(this)
        this.state = { collapse: false }
    }

    toggle() {
        this.setState({ collapse: !this.state.collapse })
    }

    render() {
        const { employee } = this.props

        return (
            <div className="container">
                <div className="row">
                    {employee.length > 0 &&
                        employee.map(emp => {
                            return (
                                <div className="col-md-4" key={emp.id}>
                                    <Card>
                                        <CardHeader>
                                            <strong>Name:</strong> {emp.name}
                                        </CardHeader>
                                        <CardBody>
                                            <CardText>
                                                <strong>Age: </strong>
                                                {emp.age}
                                            </CardText>
                                            <CardText>
                                                <strong>Email: </strong>
                                                {emp.email}
                                            </CardText>
                                            <CardText>
                                                <strong>Address: </strong>
                                                {emp.address}
                                            </CardText>
                                            <CardText>
                                                <strong>Experience: </strong>
                                                {emp.experience}
                                            </CardText>
                                            <CardText>
                                                <strong>Salary: </strong>
                                                {emp.salary}
                                            </CardText>
                                            <CardText>
                                                <strong>Date of Joining: </strong>
                                                {emp.doj}
                                            </CardText>
                                        </CardBody>
                                    </Card>
                                    <Button
                                        onClick={() => this.props.removeEmployee(emp.id)}
                                        className="btn-block mt-2"
                                    >
                                        Delete
                                    </Button>
                                    <UpdateForm emp={emp.id} />
                                </div>
                            )
                        })}
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    employee: state.employee
})

export default connect(
    mapStateToProps,
    { removeEmployee }
)(EmployeeList)
