import React, { Component } from 'react'
import Layout from './Layout'
import CreateForm from './CreateForm'
import EmployeeList from './EmployeeList'

class HomePage extends Component {
    render() {
        return (
            <Layout>
                <CreateForm />
                <EmployeeList />
            </Layout>
        )
    }
}

export default HomePage
