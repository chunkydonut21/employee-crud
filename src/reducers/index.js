import { combineReducers } from 'redux'
import employeeReducer from './employeeReducer'
import { reducer as reduxForm } from 'redux-form'

export default combineReducers({
    employee: employeeReducer,
    form: reduxForm
})
