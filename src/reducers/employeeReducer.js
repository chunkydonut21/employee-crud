import { ADD_EMPLOYEE, REMOVE_EMPLOYEE, UPDATE_EMPLOYEE } from '../actions'

const INITIAL_STATE = []

const reducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case ADD_EMPLOYEE:
            return [...state, action.payload]

        case UPDATE_EMPLOYEE:
            return state.map(emp => {
                if (emp.id === action.payload.id) {
                    return action.payload
                } else {
                    return emp
                }
            })

        case REMOVE_EMPLOYEE:
            return state.filter(emp => {
                return emp.id !== action.payload
            })

        default:
            return state
    }
}

export default reducer
