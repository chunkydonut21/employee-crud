export const ADD_EMPLOYEE = 'add_employee'
export const REMOVE_EMPLOYEE = 'remove_employee'
export const UPDATE_EMPLOYEE = 'update_employee'

export const addEmployee = val => {
    return {
        type: ADD_EMPLOYEE,
        payload: val
    }
}

export const removeEmployee = id => {
    return {
        type: REMOVE_EMPLOYEE,
        payload: id
    }
}

export const updateEmployee = val => {
    return {
        type: UPDATE_EMPLOYEE,
        payload: val
    }
}
